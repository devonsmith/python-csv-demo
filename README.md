# README #

Python CSV Demonstration

### What is this repository for? ###

* This script was created for fun. This will read the information from a tab delimited file and create a new file or append to an existing file with a selected subset of the columns.
* This script was created for demonstration puposes.
* Version 0.1


### How do I get set up? ###

* Uses python 3 should not require any special libraries or tools.

## Notes ##
* The demo.csv file is used to demonstrate the functionality of this tool and the libraries it uses. The URLs contained in this file are fictitious and should not be visited. If these are real addresses they may not be safe to visit. 