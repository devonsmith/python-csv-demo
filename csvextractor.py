###############################################################################
##                                                                           ##
## CSV Processing Demo Application                                           ##
## Devon Smith                                                               ##
##                                                                           ##
## Purpose: Demonstrates processing information from a tab delimited CSV     ##
##          file. Reads from a CSV file and extracts some selected           ##
##          information from the file and places it into a new file.         ##
##                                                                           ##
###############################################################################

import sys
import csv
import os.path


## MAIN ##
def main():
    # Get command line arguments
    args = sys.argv[1:]

    # Did the user provide enough command-line arguments?
    if len(args) >= 2:
        # Get the input file.
        inputFile = args[0]
        del args[0:1]

        # Get the output file path.
        outputFile = args[0]
        del args[0:1]
    else:
        print("Usage [command] inputfile outputfile")
        sys.exit()

    #Open the input file and turn it into a list of dictionaries
    with open(inputFile) as inFile:
        fileContent = list(csv.DictReader(inFile, dialect='excel-tab'))

    ''' checks to see if a file exists and if the file does, it will append
        to that file. If the file does not exist it will create a new file that
        includes the header information for the CSV file dialect.'''
    if (os.path.exists(outputFile) is True):
        # Open the file with an append flag.
        with open(outputFile, 'a') as newFile:
            writer = csv.DictWriter(newFile,
                                    fieldnames=['Name', 'Url'],
                                    dialect='excel-tab')
            # Write the elements to the file.
            for row in fileContent:
                writer.writerow({'Name': row['Name'], 'Url': row['Url']})
    else:
        # Open the file with an exclusive create flag.
        with open(outputFile, 'x') as newFile:
            writer = csv.DictWriter(newFile,
                                    fieldnames=['Name', 'Url'],
                                    dialect='excel-tab')
            # Write the head of the CSV file.
            writer.writeheader()
            # Write the elements to the file.
            for row in fileContent:
                writer.writerow({'Name': row['Name'], 'Url': row['Url']})

# Start the application
if __name__ == '__main__':
    main()
